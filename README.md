# Beurer BF-100 & BF-105 - USB data retriever and parser

This project parses the binary data from a Beurer BF-100 scale, retrieved using [this python script by rwrnet](https://github.com/rwrnet/BeurerScaleManagerCLI/blob/master/scale.py).

In the [branch bf105](https://gitlab.com/thomas351/beurer_bf100_parser/-/blob/bf105/src/main.rs) you'll find a more advanced version that also incooperates the usb data retrieval with a [clap derive CLI](https://docs.rs/clap/latest/clap/_derive/).

Before starting this project i've also found this [C++ Qt GUI project](https://github.com/Urban82/BeurerScaleManager) for the Beurer BF 480 scale model (may also work for other models that don't have a hand-held sensor and store the last 60 measurements instead of only the last 30 like Beurer BF-100 & BF-105 do)

Urban82, the creator of that GUI project also did the hard work of [analysing the binary layout](https://github.com/Urban82/BeurerScaleManager/wiki/Analysis) on which I based the slightly different binary parsing layouts of the models BF-100 & BF-105 you find in this repository.
