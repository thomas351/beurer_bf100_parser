# https://www.phind.com/search?cache=sawheh3scdqe3shf6ieienbx
# ID 04d9:8010 Holtek Semiconductor, Inc. USB-HID demo code
echo 'SUBSYSTEMS=="usb", ATTRS{idVendor}=="04d9", ATTRS{idProduct}=="8010", GROUP="user", MODE="0660", TAG+="uaccess"' > /etc/udev/rules.d/99-beurer_scales.rules
udevadm control --reload-rules
