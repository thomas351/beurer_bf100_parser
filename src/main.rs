use binread::{BinRead, BinReaderExt, BinResult, ReadOptions};
use std::fs::File;
use std::io::{BufReader, Read, Seek};

#[derive(Debug, BinRead, Default)]
struct Measurements {
    weights: [u16; 30],

    #[br(pad_before = 4)] // 4 bytes = 00
    fat_avg: [u16; 30],

    #[br(pad_before = 4)] // 4 bytes = 00
    water_avg: [u16; 30],

    #[br(pad_before = 4)] // 4 bytes = 00
    muscle_avg: [u16; 30],

    #[br(pad_before = 4)] // 4 bytes = 00
    bone_hg: [u16; 30],

    #[br(pad_before = 4, parse_with = decode_dates)] // 4 bytes = 00
    date: Vec<Date>,

    #[br(pad_before = 4)] // 4 bytes = 00
    fat_top: [u16; 30],

    #[br(pad_before = 4)] // 4 bytes = 00
    fat_bottom: [u16; 30],

    #[br(pad_before = 4)] // 4 bytes = 00
    muscle_top: [u16; 30],

    #[br(pad_before = 4)] // 4 bytes = 00
    muscle_bottom: [u16; 30],

    #[br(pad_before = 4)] // 4 bytes = 00
    basal_metabolic_rate_kcal: [u16; 30],

    #[br(pad_before = 4, pad_after = 4)] // 4 bytes = 00
    active_metabolic_rate_kcal: [u16; 30],
}

#[derive(Debug, Default)]
struct Date {
    year: u16,
    month: u8,
    day: u8,
}

#[derive(Debug, BinRead, Default)]
struct User {
    id: u8,
    height: u8,
    #[br(parse_with = decode_date)]
    birthday: Date,
    gender_and_activity: u8,
    measurement_count: u8,
    _unknown: [u8; 2],
}

#[derive(Debug, BinRead)]
struct Data {
    measurements: [Measurements; 10],

    #[br(pad_before = 256)] // 256 bytes = FF
    user: [User; 10],

    #[br(pad_before = 144)] // 1 byte = 00 & 143 bytes = FF
    _unknown: [u8; 10],
    // all_fs: [u8; 22],
}

fn decode_date<R: Read + Seek>(reader: &mut R, _: &ReadOptions, _: ()) -> BinResult<Date> {
    let value = reader.read_be::<u16>()?;
    let year = 1920 + ((value >> 9) & 0b1111111);
    let month = ((value >> 5) & 0b1111) as u8;
    let day = (value & 0b11111) as u8;
    Ok(Date { year, month, day })
}

fn decode_dates<R: Read + Seek>(reader: &mut R, ro: &ReadOptions, _: ()) -> BinResult<Vec<Date>> {
    let mut dates = Vec::new();
    for _ in 0..30 {
        dates.push(decode_date(reader, ro, ())?);
    }
    Ok(dates)
}

fn main() {
    // Open the binary file
    let file = File::open("dumpFile.bin").expect("Unable to open file");
    let mut reader = BufReader::new(file);

    // Read and parse the binary file into the Header struct
    let data: Data = reader.read_be().expect("Unable to read file");

    println!("\n\nuser_id;height;birthdate;gender_and_activity;measurement_count");

    for user in &data.user {
        if user.id == 0 {
            break;
        }
        println!(
            "{};{};{}-{:02}-{:02};{};{}",
            user.id,
            user.height,
            user.birthday.year,
            user.birthday.month,
            user.birthday.day,
            user.gender_and_activity,
            user.measurement_count
        );
    }

    for (index, user) in data.user.iter().enumerate() {
        if user.id == 0 {
            break;
        }
        println!(
            "\n\n{} measurements of user #{} (born {}-{:02}-{:02}, height {}cm):",
            user.measurement_count, user.id, user.birthday.year, user.birthday.month, user.birthday.day, user.height,
        );
        println!("date;weight_kg;fat_avg_pct;water_avg_pct;muscle_avg_pct;bone_kg;active_metabolic_rate_kcal;basal_metabolic_rate_kcal;fat_top_pct;fat_bottom_pct;muscle_top_pct;muscle_bottom_pct");
        let measurements = &data.measurements[index];
        for m in (0..user.measurement_count as usize).rev() {
            let date = measurements.date.get(m).unwrap();
            println!(
                "{}-{:02}-{:02};{};{};{};{};{};{};{};{};{};{};{}",
                date.year,
                date.month,
                date.day,
                measurements.weights[m] as f32 / 10.0,
                measurements.fat_avg[m] as f32 / 1000.0,
                measurements.water_avg[m] as f32 / 1000.0,
                measurements.muscle_avg[m] as f32 / 1000.0,
                measurements.bone_hg[m] as f32 / 10.0,
                measurements.active_metabolic_rate_kcal[m],
                measurements.basal_metabolic_rate_kcal[m],
                measurements.fat_top[m] as f32 / 1000.0,
                measurements.fat_bottom[m] as f32 / 1000.0,
                measurements.muscle_top[m] as f32 / 1000.0,
                measurements.muscle_bottom[m] as f32 / 1000.0,
            );
        }
    }
    println!("\n");
}
